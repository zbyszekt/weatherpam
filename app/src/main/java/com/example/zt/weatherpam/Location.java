package com.example.zt.weatherpam;

import java.io.Serializable;

/**
 * Created by zt on 25.04.15.
 */
public class Location implements Serializable {

     Query query;

    public static class Query implements Serializable{
        Results results;

        public Results getResult() {
            return results;
        }

        public void setResult(Results result) {
            this.results = result;
        }
    }

    public static class Results implements Serializable{
        public Place place;

        public Place getPlace() {
            return place;
        }

        public void setPlace(Place place) {
            this.place = place;
        }
    }

    public static class Place implements Serializable{
        String name;
        String  woeid;
        Country country;
        Admin admin1;
        Admin admin2;
        Admin admin3;
        Locality locality1;
        Postal postal;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getWoeid() {
            return woeid;
        }

        public void setWoeid(String woeid) {
            this.woeid = woeid;
        }

        public Country getCountry() {
            return country;
        }

        public void setCountry(Country country) {
            this.country = country;
        }

        public Admin getAdmin1() {
            return admin1;
        }

        public void setAdmin1(Admin admin1) {
            this.admin1 = admin1;
        }

        public Admin getAdmin2() {
            return admin2;
        }

        public void setAdmin2(Admin admin2) {
            this.admin2 = admin2;
        }

        public Admin getAdmin3() {
            return admin3;
        }

        public void setAdmin3(Admin admin3) {
            this.admin3 = admin3;
        }

        public Locality getLocality1() {
            return locality1;
        }

        public void setLocality1(Locality locality1) {
            this.locality1 = locality1;
        }

        public Postal getPostal() {
            return postal;
        }

        public void setPostal(Postal postal) {
            this.postal = postal;
        }
    }

    public static class Country implements Serializable{
        String code;
        String content;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class Admin implements Serializable{
        String code;
        String content;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class Locality implements Serializable{
        String type;
        String content;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class Postal implements Serializable{
        String content;

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }
}
