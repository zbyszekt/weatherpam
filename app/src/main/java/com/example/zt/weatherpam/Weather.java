package com.example.zt.weatherpam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zt on 15.04.15.
 */
public class Weather implements Serializable {

    public Query getQuery() {
        return query;
    }

    public void setQuery(Query query) {
        this.query = query;
    }

    private Query query;

    public static class Query implements Serializable{
        private Results results;

        public Results getResults() {
            return results;
        }

        public void setResults(Results results) {
            this.results = results;
        }
    }

    public static class Results implements Serializable{
        public Channel getChannel() {
            return channel;
        }

        public void setChannel(Channel channel) {
            this.channel = channel;
        }

        private Channel channel;
    }

    public static class Channel implements Serializable{
        private Location location;
        private Units units;
        private Wind wind;
        private Atmosphere atmosphere;
        private Astronomy astronomy;
        private Item item;
        private String title;
        private int ttl;
        private String lastBuildDate;

        public String getLastBuildDate() {
            return lastBuildDate;
        }

        public void setLastBuildDate(String lastBuildDate) {
            this.lastBuildDate = lastBuildDate;
        }

        public int getTtl() {
            return ttl;
        }

        public void setTtl(int ttl) {
            this.ttl = ttl;
        }



        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Location getLocation() {
            return location;
        }

        public void setLocation(Location location) {
            this.location = location;
        }

        public Units getUnits() {
            return units;
        }

        public void setUnits(Units units) {
            this.units = units;
        }

        public Wind getWind() {
            return wind;
        }

        public void setWind(Wind wind) {
            this.wind = wind;
        }

        public Atmosphere getAtmosphere() {
            return atmosphere;
        }

        public void setAtmosphere(Atmosphere atmosphere) {
            this.atmosphere = atmosphere;
        }

        public Astronomy getAstronomy() {
            return astronomy;
        }

        public void setAstronomy(Astronomy astronomy) {
            this.astronomy = astronomy;
        }

        public Item getItem() {
            return item;
        }

        public void setItem(Item item) {
            this.item = item;
        }
    }

    public static class Location implements Serializable{
        private String city;
        private String country;
        private String region;

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getRegion() {
            return region;
        }

        public void setRegion(String region) {
            this.region = region;
        }
    }

    public static class Units implements Serializable{

        private String distance;
        private String pressure;
        private String temperature;

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public String getPressure() {
            return pressure;
        }

        public void setPressure(String pressure) {
            this.pressure = pressure;
        }

        public String getTemperature() {
            return temperature;
        }

        public void setTemperature(String temperature) {
            this.temperature = temperature;
        }
    }

    public static class Wind implements Serializable{
        private int chill;
        private int direction;
        private float speed;

        public int getChill() {
            return chill;
        }

        public void setChill(int chill) {
            this.chill = chill;
        }

        public int getDirection() {
            return direction;
        }

        public void setDirection(int direction) {
            this.direction = direction;
        }

        public float getSpeed() {
            return speed;
        }

        public void setSpeed(float speed) {
            this.speed = speed;
        }
    }

    public static class Atmosphere implements Serializable{
        private int humidity;
        private double visibility;
        private double pressure;
        private int rising;

        public int getHumidity() {
            return humidity;
        }

        public void setHumidity(int humidity) {
            this.humidity = humidity;
        }

        public double getVisibility() {
            return visibility;
        }

        public void setVisibility(double visibility) {
            this.visibility = visibility;
        }

        public double getPressure() {
            return pressure;
        }

        public void setPressure(double pressure) {
            this.pressure = pressure;
        }

        public int getRising() {
            return rising;
        }

        public void setRising(int rising) {
            this.rising = rising;
        }
    }

    public static class Astronomy implements Serializable{
        private String sunrise;
        private String sunset;

        public String getSunrise() {
            return sunrise;
        }

        public void setSunrise(String sunrise) {
            this.sunrise = sunrise;
        }

        public String getSunset() {
            return sunset;
        }

        public void setSunset(String sunset) {
            this.sunset = sunset;
        }
    }

    public static class Item implements Serializable {
        private Condition condition;
        private ArrayList<Forecast> forecast;

        public Condition getCondition() {
            return condition;
        }

        public void setCondition(Condition condition) {
            this.condition = condition;
        }

        public ArrayList<Forecast> getForecast() {
            return forecast;
        }

        public void setForecast(ArrayList<Forecast> forecast) {
            this.forecast = forecast;
        }
    }


    public  static class Condition implements Serializable{
        private String  text;
        private int code;
        private int temp;
        private String date;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        public int getTemp() {
            return temp;
        }

        public void setTemp(int temp) {
            this.temp = temp;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }
    }

    public  static class Forecast implements Serializable{
        private String day;
        private String date;
        private int low;
        private int high;
        private int code;

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        private String text;

        public String getDay() {
            return day;
        }

        public void setDay(String day) {
            this.day = day;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getLow() {
            return low;
        }

        public void setLow(int low) {
            this.low = low;
        }

        public int getHigh() {
            return high;
        }

        public void setHigh(int high) {
            this.high = high;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }
    }
}
