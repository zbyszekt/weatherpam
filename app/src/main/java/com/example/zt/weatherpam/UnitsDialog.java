package com.example.zt.weatherpam;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.Toast;


public class UnitsDialog extends DialogFragment {


    public UnitsDialog() {
        // Required empty public constructor
    }

    UnitChange listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try{
            listener = (UnitChange)activity;
        }
        catch (ClassCastException e ){
            e.printStackTrace();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setTitle(getString(R.id.unit_title)).setItems(R.array.units,new DialogInterface.OnClickListener(){


            @Override
            public void onClick(DialogInterface dialog, int which) {
                SharedPreferences pref = getActivity().getPreferences(Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putInt(getString(R.string.default_unit), which);
                editor.commit();
                listener.unitsChanged();
            }
        });
        return builder.create();
    }

    public interface UnitChange{
        void unitsChanged();

    }
}
