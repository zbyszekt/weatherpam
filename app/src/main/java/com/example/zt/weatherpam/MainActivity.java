package com.example.zt.weatherpam;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;


public class MainActivity extends ActionBarActivity implements ChoosePlaceFragment.CityChoosingListener,UnitsDialog.UnitChange {
    public final static String FILENAME = "weather.json";
    private static final String WOOEID = "woeid";

    private ViewPager pager;
    private PagerAdapter pagerAdapter;
    private ProgressBar progres;
    private ArrayList<Fragment> fragments;
    // Fragment overview, details;
    ChoosePlaceFragment choosePlaceFragment;
    Weather weather;
    View fragmentContainer;
    private int wooeid;

    boolean large = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        fragmentContainer = findViewById(R.id.fragment_container);
        if (fragmentContainer != null) {
            large = true;
        } else {
            large = false;
            fragments = new ArrayList<>();
            progres = (ProgressBar) findViewById(R.id.progressBar);
            pager = (ViewPager) findViewById(R.id.pager);
            pagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
            pager.setAdapter(pagerAdapter);
        }


        if (fileExistance(FILENAME)) {
            loadWeatherFromFile();
        } else {
            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            wooeid = sharedPref.getInt(WOOEID, -1);
            if (wooeid < 0) {
                updateLocation();
                return;
            } else {
                loadWeatherFromWeb();
            }
        }


    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    void updateLocation(){

        choosePlaceFragment = new ChoosePlaceFragment();
        choosePlaceFragment.show(getSupportFragmentManager(),null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.update:
                if (!loadWeatherFromWeb())
                    Toast.makeText(this, getString(R.string.no_network), Toast.LENGTH_LONG).show();
                else {
                    Toast.makeText(this, getString(R.string.succes), Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.changeLocation:
                updateLocation();
                return true;
            case R.id.units:
                UnitsDialog unit = new UnitsDialog();
                unit.show(getSupportFragmentManager(), "fragment");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private boolean loadWeatherFromWeb() {
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {

            SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
            int u = pref.getInt(getString(R.string.default_unit), 0);
            wooeid = pref.getInt(WOOEID, -1);

            String units = u == 0 ? "c" : "f";
            String address = "https://query.yahooapis.com/v1/public/yql?q=";
            String query = "select * from weather.forecast where woeid=" + wooeid + " and u=\"" + units + "\"";
            String format = "&format=json";
            new DownloadWeatherTask().execute(address + URLEncoder.encode(query) + format);
            return true;
        }

        return false;
    }

    private boolean loadWeatherFromFile() {
        if (fileExistance(FILENAME)) {
            try {
                FileInputStream input = openFileInput(FILENAME);
                String s = IOUtils.toString(input);
                weather = new Gson().fromJson(s, Weather.class);
                if (!weather.getQuery().getResults().getChannel().getTitle().toLowerCase().contains("error")) {
                    updateViews(
                            OverViewFragment.newInstance(weather),
                            DetailsFragment.newInstance(weather),
                            ForecastFragment.newInstance(weather.getQuery().getResults().getChannel().getItem().getForecast())


                    );

                    try {
                        DateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy h:mm a", Locale.US);
                        String t = weather.getQuery().getResults().getChannel().getItem().getCondition().getDate();
                        DateTime dateTime = new DateTime(dateParser.parse(t));
                        //dateTime.withZone(DateTimeZone.forTimeZone())
                        int ttl = weather.getQuery().getResults().getChannel().getTtl();
                        dateTime = dateTime.plusMinutes(ttl);
                        Log.d("date", dateTime.toString());
                        if (dateTime.isBeforeNow()) {
                            Toast.makeText(this, R.string.expiration_warn, Toast.LENGTH_LONG).show();
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }


                    return true;
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public void selectedCity(int woeid) {
        this.wooeid = woeid;
        //save preferences
        SharedPreferences pref = getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt(WOOEID, wooeid);
        editor.commit();
        if (!loadWeatherFromWeb()) {
            Toast.makeText(this, R.string.no_network, Toast.LENGTH_SHORT);
        }
        choosePlaceFragment.dismiss();

        ;
    }

    public boolean fileExistance(String fname) {
        File file = getFileStreamPath(fname);
        return file.exists();
    }


    void updateViews(Fragment... args) {
        if (!large) {
            fragments.clear();
            for (Fragment x : args) {
                fragments.add(x);
            }
            pagerAdapter.notifyDataSetChanged();
            pager.setVisibility(View.VISIBLE);
            progres.setVisibility(View.GONE);
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.large_overview, args[0]).replace(R.id.large_details, args[1]).replace(R.id.large_forecast,args[2]).commit();

        }
    }

    @Override
    public void unitsChanged() {
        if (!loadWeatherFromWeb()){
            Toast.makeText(this,R.string.change_units,Toast.LENGTH_LONG).show();
        }
    }


    private class MyPagerAdapter extends FragmentStatePagerAdapter {
        public MyPagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }

        @Override
        public Fragment getItem(int i) {
            return fragments.get(i);
        }


        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {

        }

        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }
    }

    private class DownloadWeatherTask extends AsyncTask<String, Void, String> {

        private static final String DEBUG_TAG = "pogoda";
        private static final String FILES = "file";
        private int response;

        @Override
        protected String doInBackground(String... params) {
            try {
                return downloadUrl(params[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        private String downloadUrl(String myurl) throws IOException {

            InputStream is = null;


            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                response = conn.getResponseCode();
                Log.d(DEBUG_TAG, "The response is: " + response);
                is = conn.getInputStream();

                return IOUtils.toString(is, "UTF-8");

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return "Sth wrng";
        }


        @Override
        protected void onPostExecute(String s) {
            //Log.d("pogoda",s);
            if (response == 200) {

                weather = new Gson().fromJson(s, Weather.class);

                if (!weather.getQuery().getResults().getChannel().getTitle().toLowerCase().contains("error")) {
                    FileOutputStream out = null;
                    try {
                        out = openFileOutput(FILENAME, Context.MODE_PRIVATE);
                        out.write(s.getBytes());
                        out.close();
                    } catch (FileNotFoundException e) {
                        Log.d(FILES, "FileNotFound writing");
                    } catch (IOException e) {
                        Log.d(FILES, "IOException writing");
                    } finally {
                        if (out != null) {
                            try {
                                out.close();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                    }

                    updateViews(OverViewFragment.newInstance(weather), DetailsFragment.newInstance(weather), ForecastFragment.newInstance(weather.getQuery().getResults().getChannel().getItem().getForecast()));
                }
            }

            // allView.
        }

    }
}
