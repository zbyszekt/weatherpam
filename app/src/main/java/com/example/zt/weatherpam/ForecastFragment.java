package com.example.zt.weatherpam;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ForecastFragment extends Fragment {

    private static final String FORECAST = "FORECAST";
    private  List<Weather.Forecast> itemList;
    private GridView grid;
    public ForecastFragment() {
        // Required empty public constructor
    }

    public static ForecastFragment newInstance(ArrayList<Weather.Forecast> list){
        ForecastFragment f = new ForecastFragment();
        Bundle args = new Bundle();
        args.putSerializable(FORECAST,list);
        f.setArguments(args);
        return f;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null){
            Bundle args  = getArguments();
            itemList = (List<Weather.Forecast>) args.getSerializable(FORECAST);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forecast, container, false);
        grid = (GridView) view.findViewById(R.id.gridView);
        grid.setAdapter(new GridAdapter(getActivity()));
        return view;
    }



    private class GridAdapter  extends BaseAdapter{
    private Context context;
       GridAdapter(Context context){
           this.context = context;
       }
       @Override
       public int getCount() {
           return itemList.size();
       }

       @Override
       public Object getItem(int position) {
           return itemList.get(position);
       }

       @Override
       public long getItemId(int position) {
           return position;
       }

       @Override
       public View getView(int position, View convertView, ViewGroup parent) {
           if (convertView == null) {
               LayoutInflater inflater = LayoutInflater.from(context);
               convertView = inflater.inflate(R.layout.forecast_item, parent, false);
           }
           TextView date = (TextView) convertView.findViewById(R.id.date);
           ImageView image = (ImageView) convertView.findViewById(R.id.image);
           TextView hilow = (TextView) convertView.findViewById(R.id.hilow);
           TextView text = (TextView) convertView.findViewById(R.id.text);
           ////////////////////////////////
           Weather.Forecast item = itemList.get(position);
           date.setText(item.getDate());
           image.setImageResource(OverViewFragment.images[item.getCode()]);
           hilow.setText(item.getHigh()+"/"+item.getLow());
           text.setText(item.getText());
           return  convertView;
       }
   }



}
