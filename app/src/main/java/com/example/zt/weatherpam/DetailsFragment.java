package com.example.zt.weatherpam;


import android.media.Image;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetailsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsFragment extends Fragment {

    private static final String CHILL = "chill";
    private static final String DIRECTION = "dir";
    private static final String SPEED = "speed";
    private static final String HUMIDITY = "hum";
    private static final String VISIBILITY = "visi";
    private static final String PRESSURE = "pressure" ;
    private static final String degrees = "\u00B0";
    private static final String perHour = "/h";
    private static final String SUNRISE = "sunrise";
    private static final String SUNSET = "sunset";


    public static DetailsFragment newInstance(Weather weather) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        Weather.Channel channel = weather.getQuery().getResults().getChannel();
        Weather.Wind wind = channel.getWind();
        Weather.Atmosphere atm = channel.getAtmosphere();
        args.putString(CHILL,wind.getChill()+degrees+channel.getUnits().getTemperature());
        args.putInt(DIRECTION, wind.getDirection());
        args.putString(SPEED, wind.getSpeed() + channel.getUnits().getDistance() + perHour);
        args.putString(HUMIDITY, atm.getHumidity() + "%");
        args.putString(VISIBILITY, atm.getVisibility() * 10 + "%");
        args.putString(PRESSURE, atm.getPressure() + channel.getUnits().getPressure());
        args.putString(SUNRISE, channel.getAstronomy().getSunrise());
        args.putString(SUNSET, channel.getAstronomy().getSunset());
        fragment.setArguments(args);
        return fragment;
    }

    public DetailsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_details, container, false);
        if (getArguments() != null) {
            TextView chill, speed, humidity, visibility, pressure,sunrise,sunset;
            ImageView direction;

            chill = (TextView) view.findViewById(R.id.chill);
            speed = (TextView) view.findViewById(R.id.speed);
            humidity = (TextView) view.findViewById(R.id.humidity);
            visibility = (TextView) view.findViewById(R.id.visibility);
            pressure = (TextView) view.findViewById(R.id.pressure);
            sunrise = (TextView) view.findViewById(R.id.sunrise);
            sunset = (TextView) view.findViewById(R.id.sunset);
            direction = (ImageView) view.findViewById(R.id.direction);

            Bundle args = getArguments();
            chill.setText(args.getString(CHILL)+"");
            speed.setText(args.getString(SPEED)+"");
            humidity.setText(args.getString(HUMIDITY)+"");
            visibility.setText(args.getString(VISIBILITY)+"");
            pressure.setText(args.getString(PRESSURE)+"");

            sunrise.setText(args.getString(SUNRISE)+"");
            sunset.setText(args.getString(SUNSET)+"");

            direction.setRotation(args.getInt(DIRECTION));
        }

        return view;
    }



}
