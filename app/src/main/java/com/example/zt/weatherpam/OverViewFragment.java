package com.example.zt.weatherpam;


import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 */
public class OverViewFragment extends Fragment {


    private static final String LOCATION = "location";
    private static final String CODE = "code";
    private static final String DESCRIPTION = "desc";
    private static final String DATE = "date";
    private static final String TEMPERATURE = "temp";
    public static final String degrees = "\u00B0";
    public static final String perHour = "/h";

    public static int images[] = {
            R.drawable.w00, R.drawable.w01, R.drawable.w02, R.drawable.w03, R.drawable.w04,
            R.drawable.w05, R.drawable.w06, R.drawable.w07, R.drawable.w08, R.drawable.w09,
            R.drawable.w10, R.drawable.w11, R.drawable.w12, R.drawable.w13, R.drawable.w14,
            R.drawable.w15, R.drawable.w16, R.drawable.w17, R.drawable.w18, R.drawable.w19,
            R.drawable.w20, R.drawable.w21, R.drawable.w22, R.drawable.w23, R.drawable.w24,
            R.drawable.w25, R.drawable.w26, R.drawable.w26, R.drawable.w27, R.drawable.w28,
            R.drawable.w30, R.drawable.w31, R.drawable.w32, R.drawable.w33, R.drawable.w34,
            R.drawable.w35, R.drawable.w36, R.drawable.w37, R.drawable.w38, R.drawable.w39,
            R.drawable.w40, R.drawable.w41, R.drawable.w42, R.drawable.w43, R.drawable.w44,
            R.drawable.w45, R.drawable.w46, R.drawable.w47
    };
    private TextView location;
    private TextView desc;
    private TextView date;
    private TextView temp;
    private ImageView code;

    public OverViewFragment() {
        // Required empty public constructor
    }

    public static OverViewFragment newInstance(Weather weather){
        OverViewFragment fragment = new OverViewFragment();
        Bundle bundle = new Bundle();
        Weather.Channel  channel = weather.getQuery().getResults().getChannel();
        bundle.putString(LOCATION,channel.getLocation().getCity()+"("+channel.getLocation().getCountry()+")");
        bundle.putInt(CODE, channel.getItem().getCondition().getCode());
        bundle.putString(TEMPERATURE, channel.getItem().getCondition().getTemp() + degrees + channel.getUnits().getTemperature());
        bundle.putString(DESCRIPTION,channel.getItem().getCondition().getText());
        bundle.putString(DATE,channel.getItem().getCondition().getDate());
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_over_view, container, false);
           Bundle args = getArguments();
        if (args != null) {
         location = (TextView) view.findViewById(R.id.location);
         desc = (TextView) view.findViewById(R.id.desc);

         date = (TextView) view.findViewById(R.id.date);
         temp = (TextView) view.findViewById(R.id.temperature);
         code = (ImageView) view.findViewById(R.id.image);


            location.setText(args.getString(LOCATION));
            desc.setText(args.getString(DESCRIPTION));
            DateFormat dateParser = new SimpleDateFormat("EEE, dd MMM yyyy h:mm a", Locale.US);
            try {
                DateTime dateTime = new DateTime(dateParser.parse(args.getString(DATE)));
                DateTimeFormatter dtt = DateTimeFormat.forPattern("h:mm a");
                DateTimeFormatter dtd = DateTimeFormat.forPattern("dd/MM/YYYY");
                date.setText(dtt.print(dateTime)+"\n"+dtd.print(dateTime));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            temp.setText(args.getString(TEMPERATURE));
            code.setImageResource(images[args.getInt(CODE)]);
        }
        return view;
    }



}
