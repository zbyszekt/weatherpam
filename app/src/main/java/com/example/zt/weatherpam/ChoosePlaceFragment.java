package com.example.zt.weatherpam;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChoosePlaceFragment extends DialogFragment {

    public ViewHolder holder = new ViewHolder();
    private  int woeid;
    private CityChoosingListener mListener;
    public ChoosePlaceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_choose_place, container, false);
        holder.search = (Button) view.findViewById(R.id.search_btn);
        holder.text = (EditText) view.findViewById(R.id.text);

        holder.result = view.findViewById(R.id.result);
        holder.country = (TextView) view.findViewById(R.id.country);
        holder.admin1 = (TextView) view.findViewById(R.id.admin1);
        holder.admin2 = (TextView) view.findViewById(R.id.admin2);
        holder.admin3 = (TextView) view.findViewById(R.id.admin3);
        holder.town = (TextView) view.findViewById(R.id.town);
        holder.postal = (TextView) view.findViewById(R.id.postal);
        holder.confirm = (Button) view.findViewById(R.id.confirm);
        holder.progress = (ProgressBar) view.findViewById(R.id.locationProgress);

        holder.search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.text.getText().length() > 0){

                    String address = "https://query.yahooapis.com/v1/public/yql?q=";
                    String query = "select * from geo.places(1) where text=\""+holder.text.getText()+"\"";
                    String format = "&format=json";
                    ConnectivityManager connMgr = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
                    NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
                    if (networkInfo != null && networkInfo.isConnected()) {
                        holder.progress.setVisibility(View.VISIBLE);
                        new DownloadLocationTask().execute(address + URLEncoder.encode(query) + format);
                    }else {
                        Toast.makeText(getActivity(),R.string.no_network,Toast.LENGTH_LONG).show();
                    }

                }
            }
        });

        holder.confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    woeid = Integer.parseInt(holder.woeid);
                    mListener.selectedCity(woeid);

                }
                catch (NumberFormatException e){
                    Log.d("D","ERROR");
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (CityChoosingListener) activity;
        }
        catch (ClassCastException e){

        }
    }

    static class ViewHolder {
        private static final String NA = "";
        Button   search,confirm;
        EditText text;
        TextView country, admin1,admin2,admin3, town,postal;
        View result;
        ProgressBar progress;
        String woeid;

        void setFields(Location location){
            if (location.query  != null && location.query.results != null) {
                Location.Results result = location.query.getResult();
                String country = result.getPlace().getCountry() != null ? result.getPlace().getCountry().getContent() : NA;
                String admin1 = result.getPlace().getAdmin1() != null ? result.getPlace().getAdmin1().getContent() : NA;
                String admin2 = result.getPlace().getAdmin2() != null ? result.getPlace().getAdmin2().getContent() : NA;
                String admin3 = result.getPlace().getAdmin3() != null ? result.getPlace().getAdmin3().getContent() : NA;
                String town = result.getPlace().getName() != null ? result.getPlace().getName() : NA;
                String postal = result.getPlace().getPostal() != null ? result.getPlace().getPostal().getContent() : NA;
                woeid = result.getPlace().getWoeid() != null ? result.getPlace().getWoeid() : NA;
                this.country.setText(country);
                this.admin1.setText(admin1);
                this.admin2.setText(admin2);
                this.admin3.setText(admin3);
                this.town.setText(town);
                this.postal.setText(postal);
                this.confirm.setEnabled(true);
            }
            else {
                this.country.setText("Unavailable location.");
                this.confirm.setEnabled(false);
            }

            this.result.setVisibility(View.VISIBLE);
            this.progress.setVisibility(View.GONE);
        }
    }


    private class DownloadLocationTask extends AsyncTask<String, Void, String> {

        private static final String DEBUG_TAG = "location";
        private int response;

        @Override
        protected String doInBackground(String... params) {
            try {
                return downloadUrl(params[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        private String downloadUrl(String myurl) throws IOException {

            InputStream is = null;


            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                response = conn.getResponseCode();
                Log.d(DEBUG_TAG, "The response is: " + response);
                is = conn.getInputStream();

                return IOUtils.toString(is, "UTF-8");

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (ProtocolException e) {
                e.printStackTrace();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return "Sth wrng";
        }


        @Override
        protected void onPostExecute(String s) {
            //Log.d("pogoda",s);
            if (response == 200) {
                Location location = new Gson().fromJson(s,Location.class);
                holder.setFields(location);
            }

            // allView.
        }

    }

    public interface CityChoosingListener{
        void selectedCity(int woeid);
    }
}
